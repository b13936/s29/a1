
const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.get("/home", (req,res) => res.send(`Welcome to Home!`));


let users = [{"username": "johndoe",
		"password": "johndoe1234"
}];


app.get("/users", (req,res) => {

	console.log(users);
	res.send(users)
});


app.delete("/delete-user", (req,res) => {

	let name = req.body.name

	res.send(`${name} was removed from the database`)
});


app.listen(PORT, () => console.log(`Server running at port ${PORT}`))